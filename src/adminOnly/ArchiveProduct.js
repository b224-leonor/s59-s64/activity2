import { useState, useContext } from 'react'
import { useParams } from 'react-router-dom'
import useGet from '../customHooks/useGet'
import { Button } from 'react-bootstrap'
import UserContext from '../UserContext'

const ProductDetails = () => {
  const [isActive, setIsActive] = useState('false')
  const { id } = useParams()
  const {
    data: product,
    error,
    isPending,
  } = useGet('https://capstone2-leonor.onrender.com/products/' + id)
  const token = localStorage.getItem('token')
  const { user } = useContext(UserContext)
  const isAdmin = Object.values(user)
  console.log(isAdmin)

  const handleClick = () => {
    setIsActive(!isActive)
    fetch(
      'https://capstone2-leonor.onrender.com/products/archive/' + product._id,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify({
          isActive,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.isActive === true) {
          console.log('product is active')
        } else if (data.isActive === false) {
          console.log('product is archived')
        }
        console.log(data)
      })
  }
  const handleClick2 = () => {
    fetch('https://capstone2-leonor.onrender.com/users/orders/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        productId: product._id,
        productName: product.name,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          alert('Order has been placed')
        }
      })
  }

  return (
    <div className='product-details'>
      {isPending && <div>Loading...</div>}
      {error && <div> {error} </div>}
      {product && (
        <article>
          <h2>{product.name}</h2>
          <p>Cost of item {product.price}</p>
          <div>{product.description}</div>
          <button onClick={handleClick} className={!isAdmin ? '' : 'hidden'}>
            Archive
          </button>
          <Button onClick={handleClick2}>Order this product</Button>
        </article>
      )}
    </div>
  )
}

export default ProductDetails
