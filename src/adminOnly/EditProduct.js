import { Row, Col, Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function EditProduct({ products, title }) {
  return (
    <div className='product-list'>
      <h2>{title}</h2>
      {products.map((product) => (
        <Row className='' key={product._id}>
          <Col xs={12} md={6}>
            <Card className='cardHighlight2 p-3'>
              <Card.Body>
                <Card.Title>{product.name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{product.description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {product.price}</Card.Text>
                <Button
                  className='bg-success'
                  as={Link}
                  to={`/products/${product._id}`}
                >
                  Deactivate
                </Button>
                <Button
                  className='bg-primary'
                  as={Link}
                  to={`/products/${product._id}`}
                >
                  Edit
                </Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      ))}
    </div>
  )
}
