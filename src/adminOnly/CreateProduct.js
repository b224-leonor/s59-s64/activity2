import { useState } from 'react'

const CreateProduct = () => {
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [price, setPrice] = useState('')

  function addProduct(e) {
    e.preventDefault()

    fetch('https://capstone2-leonor.onrender.com/products/addProduct', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data)
      })
    setName('')
    setDescription('')
    setPrice('')
  }

  return (
    <div className='create'>
      <h2>Add a New Product</h2>
      <form onSubmit={addProduct}>
        <label>Product Name</label>
        <input
          type='text'
          required
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <label>Description</label>
        <textarea
          required
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        ></textarea>
        <label>Price</label>
        <input
          type='number'
          required
          value={price}
          onChange={(e) => setPrice(e.target.value)}
        />
        <button>Add Product</button>
        <p>{name}</p>
        <p>{description}</p>
        <p>{price}</p>
      </form>
    </div>
  )
}

export default CreateProduct
