import { Row, Col, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const Home = () => {
  return (
    <div className='home'>
      <Row>
        <Col className='p-5'>
          <h2>Mountain Online store</h2>
          <p>Fresh produce from Baguio</p>
          <Link to='/products'>
            <Button variant='primary'>Order Now!</Button>
          </Link>
        </Col>
      </Row>
    </div>
  )
}

export default Home
