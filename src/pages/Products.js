import ProductCard from '../components/ProductsCard'
import useGet from '../customHooks/useGet'
import Cart from '../pages/Cart'

export default function Home() {
  const {
    data: products,
    isPending,
    error,
  } = useGet('https://capstone2-leonor.onrender.com/products/')

  return (
    <div className='products'>
      {error && <div>{error}</div>}
      {isPending && <h2>L O A D I N G. . .</h2>}
      {products && <ProductCard products={products} title='All Products' />}
      {<Cart />}
    </div>
  )
}
