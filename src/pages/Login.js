import { Form, Button } from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import { Navigate, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login() {
  const { user, setUser } = useContext(UserContext)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isActive, setIsActive] = useState(false)

  const navigate = useNavigate()

  function userLogin(e) {
    e.preventDefault()

    fetch(`https://capstone2-leonor.onrender.com/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access)
          retrieveUserDetails(data.access)

          Swal.fire({
            title: 'Start shopping?',
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Let's go!",
          }).then((result) => {
            if (result.isConfirmed) {
              Swal.fire(
                'Enjoy shopping!',
                'We got the best deal for you',
                'success'
              )
              navigate('/products')
            }
          })
        } else {
          Swal.fire({
            title: 'Authentication failed',
            icon: 'error',
            text: 'Please check your login details and try again',
          })
        }
      })

    setEmail('')
    setPassword('')
  }

  const retrieveUserDetails = (token) => {
    fetch('https://capstone2-leonor.onrender.com/users/details', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        })
      })
  }

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password])

  return user.id !== null && user.isAdmin === true ? (
    <Navigate to='/adminDashboard' />
  ) : (
    <Form onSubmit={(e) => userLogin(e)}>
      <Form.Group className='mb-3' controlId='email'>
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type='email'
          placeholder='Enter email'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          autoComplete='off'
          required
        />
      </Form.Group>
      <Form.Group className='mb-3' controlId='password'>
        <Form.Label>Password</Form.Label>
        <Form.Control
          type='password'
          placeholder='Password'
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          autoComplete='off'
          required
        />
      </Form.Group>

      {isActive ? (
        <Button variant='success' type='submit'>
          Submit
        </Button>
      ) : (
        <Button variant='success' type='submit' disabled>
          Submit
        </Button>
      )}
    </Form>
  )
}
