import { Link } from 'react-router-dom'
import useGet from '../customHooks/useGet'
import EditProduct from '../adminOnly/EditProduct'
import { Button } from 'react-bootstrap'

export default function AdminDashboard() {
  const {
    data: products,
    isPending,
    error,
  } = useGet('https://capstone2-leonor.onrender.com/products/')

  return (
    <>
      <div>
        <h2>Admin Dashboard</h2>
        <Button variant='outline-primary'>
          <Link to='/createProduct' style={{ textDecoration: 'none' }}>
            Add New Product
          </Link>
        </Button>{' '}
        <Button variant='success'>Show User Order</Button>{' '}
      </div>
      <div className='products'>
        {error && <div>{error}</div>}
        {isPending && <h2>L O A D I N G. . .</h2>}
        {products && <EditProduct products={products} title='All Products' />}
      </div>
    </>
  )
}
