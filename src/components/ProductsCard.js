import { Row, Col, Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import { useContext } from 'react'

export default function ProductCard({ products, title }) {
  const productImages = [
    { src: 'images/burger.png' },
    { src: 'images/chicken.png' },
  ]
  const { user, setUser } = useContext(UserContext)
  const token = localStorage.getItem('token')

  const handleClick = () => {
    Swal.fire({
      title: 'Sweet!',
      text: 'Modal with a custom image.',
      imageUrl: 'images/burger.png',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }

  return (
    <div className='product-list'>
      <h2>{title}</h2>
      {products.map((product) => (
        <Row className='' key={product._id}>
          <Col xs={12} md={6}>
            <Card className='cardHighlight2 p-3'>
              <Card.Body>
                <Card.Title>{product.name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{product.description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {product.price}</Card.Text>
                <Button onClick={handleClick}>Details</Button>
                <Button
                  className='bg-success'
                  as={Link}
                  to={`/products/${product._id}`}
                >
                  Add to Cart
                </Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      ))}
    </div>
  )
}
